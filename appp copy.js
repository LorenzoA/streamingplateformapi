const myKey = "33e6fcf2d04dd1afe8511c8f0b862749";
const myUrlresearch = `https://api.themoviedb.org/3/search/movie?api_key=33e6fcf2d04dd1afe8511c8f0b862749`;
const myUrlpopularity = `https://api.themoviedb.org/3/discover/movie?api_key=33e6fcf2d04dd1afe8511c8f0b862749&language=fr-US&sort_by=popularity.desc&include_adult=false&include_video=false&page=1`;

const btn = document.querySelector('#bouton');
const userChoice = document.querySelector('#valInput');
const searchingArea = document.querySelector('#searching');
const containerPopularity = document.querySelector('#container')
const contenant = document.createElement('div');
contenant.setAttribute('class', 'contenant');

function displayHomePage() {

    fetch(myUrlpopularity)
        .then((response) => response.json())
        .then((dataPopularity) => {
            const movieesPopularity = dataPopularity.results;
            //je transforme la fonction createHomed en constante pour pouvoir la passer en argument de appendChild()
            const createHmePage = createMoviesByPop(movieesPopularity);
            containerPopularity.appendChild(createHmePage);
            console.log(movieesPopularity);
        });

};
function createMoviesByPop(movieesPopularity) {
    const homePage = document.createElement('div');
    homePage.setAttribute('class', 'homepage');
    //creation d'une const qui représente mon bloc html.
    const homepageHtml = `
    <div class="contenu">
        ${movieesPopularity.map((movieesP) => {
        return `
            <div class="affiche">
                <img src="http://image.tmdb.org/t/p/w300${movieesP.poster_path}" alt="">
                <button type="button" class="btn btn-light" data-bs-toggle="modal" data-bs-target="#id${movieesP.id}">
                    En savoir plus
                </button> 
            </div>
            <div class="modal fade" id="id${movieesP.id}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog .modal-dialog-scrollable">
                    <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"><p class="h4">${movieesP.title}</p></h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="affiche">
                        <img src="http://image.tmdb.org/t/p/w300${movieesP.poster_path}" alt="">    
                        </div>
                        <div class="note">
                            <p>${movieesP.vote_average}/10</p>
                        </div>
                        <div class="genre">
                            <p>${movieesP.genre}</p>
                        </div>
                        <div class="resume">
                            <p>${movieesP.overview}</p>
                        </div>
                        <div class="date">
            
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fermer</button>
                    </div>
                    </div>
                </div>
            </div>

        `;
    })}
    </div>
    
    `;
    // Suite du dom
    homePage.innerHTML = homepageHtml;
    return homePage;
}

function createSearchArea(moviees) {



    //fonction map, me permet de boucler dans le tableau movie et de creer le html pour chaque film
    const contenu = `
    <div class="contenu">
    ${moviees.map((movie) => {
        return `
        <div class="affiche">
            <img src="http://image.tmdb.org/t/p/w300${movie.poster_path}" alt=""> 
        
        <button type="button" class="btn btn-light" data-bs-toggle="modal" data-bs-target="#id${movie.id}">
                    En savoir plus
                </button> 
            </div>
            <div class="modal fade" id="id${movie.id}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog .modal-dialog-scrollable">
                    <div class="modal-content">
                    <div class="modal-header py-auto px-auto">
                        <h5 class="modal-title " id="exampleModalLabel">${movie.title}</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="affiche">
                        <img src="http://image.tmdb.org/t/p/w300${movie.poster_path}" alt="">    
                        </div>
                        <div class="note">
                            <p>${movie.vote_average}/10</p>
                        </div>
                        <div class="genre">
                            <p>${movie.genre}</p>
                        </div>
                        <div class="resume">
                            <p>${movie.overview}</p>
                        </div>
                        <div class="date">
            
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fermer</button>
                    </div>
                    </div>
                </div>
            </div>

        `;
    })}
    </div>
    
    `;
    // Suite du dom
    contenant.innerHTML = contenu;
    return contenant;
};
//fonction recherche, à partir de l'input 
btn.onclick = function (event) {

    event.preventDefault();

    const valInput = userChoice.value;
    const searchUrl = myUrlresearch + "&language=fr-US&query=" + valInput + "&page=1&include_adult=false";
    console.log(searchUrl);
    if (containerPopularity.style.display === "none" && contenant.style.display === "none") {
        containerPopularity.style.display = "block";
    } else {
        containerPopularity.style.display = "none";
    }

    //condition pour effacer les précédents résultats de recherche
    if (contenant.style.display === "none") {
        contenant.style.display = "block";
    } else {
        contenant.remove();
    }
    fetch(searchUrl)
        .then((response) => response.json())
        .then((dataSearch) => {
            const moviees = dataSearch.results;
            console.log(dataSearch);
            //je transforme la fonction createSearchArea en constante pour pouvoir la passer en argument de appendChild()
            const createIdx = createSearchArea(moviees);
            searchingArea.appendChild(createIdx);

        })


        .catch((error) => {
            console.log("oups ", error);
        });
    userChoice.value = "";
    console.log("vous avez cherché:", valInput);
}
displayHomePage();